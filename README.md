# Proyecto AWS con Terraform

Este proyecto utiliza Terraform para automatizar la creacion y configuracion de una infraestructura en AWS. La infraestructura consiste en un bucket de Amazon S3 para almacenar archivos estaticos, dos instancias EC2 conectadas a traves de un balanceador de carga (Load Balancer).

## Contenido

1. [Requisitos](#requisitos)
2. [Configuracion de Terraform](#configuracion-de-terraform)
3. [Despliegue de la Infraestructura](#despliegue-de-la-infraestructura)
4. [Acceso a la Aplicacion](#acceso-a-la-aplicacion)
5. [Destruccion de la Infraestructura](#destruccion-de-la-infraestructura)
6. [Notas Adicionales](#notas-adicionales)

## Requisitos

Tener instalado:

- [Terraform](https://www.terraform.io/downloads.html)
- Credenciales de AWS configuradas en tu máquina local.

## Configuración de Terraform

1. Clona este repositorio.

2. Configura las variables de Terraform en el archivo `variables.tf` de acuerdo a tus necesidades.

3. Ejecuta `terraform init` para inicializar tu configuracion de Terraform.

## Despliegue de la Infraestructura

1. Ejecuta `terraform apply` para crear la infraestructura en AWS.

2. Revisa los cambios propuestos y confirma escribiendo "yes" cuando se te solicite.

## Acceso a la Aplicacion

Una vez desplegado, la aplicacion estara disponible en la URL del Load Balancer. Accede a ella desde tu navegador.

```plaintext
http://URL_DEL_LOAD_BALANCER
