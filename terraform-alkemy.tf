provider "aws" {
  region = "us-east-1"  
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow inbound HTTP traffic"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web_instance" {
  count         = 2
  ami           = "ami-xxxxxxxxxxxxxxxx"  # Cambia esto a la AMI que desees usar
  instance_type = "t2.micro"

  security_group = [aws_security_group.allow_http.id]

  tags = {
    Name = "WebInstance-${count.index + 1}"
  }
}

resource "aws_lb" "example" {
  name               = "example-lb"
  internal           = false
  load_balancer_type = "application"

  enable_deletion_protection = false

  enable_http2 = true

  security_groups = [aws_security_group.allow_http.id]

  enable_cross_zone_load_balancing = true

  subnets = ["subnet-xxxxxxxxxxxxxxxx", "subnet-yyyyyyyyyyyyyyyy"]  # Cambia esto a tus subnets
  
}
